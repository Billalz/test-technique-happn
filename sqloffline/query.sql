
-- Query BigQuery

SELECT events.event_id, events.created_ts, session.session_id
FROM events
         LEFT JOIN (
    SELECT event_id, 1 + COUNTIF(delta_in_second > 60) over (order by event_id) AS session_id
    FROM (
             SELECT event_id,
                    FIRST_VALUE(created_ts)
                    OVER ( ORDER BY event_id
                        RANGE BETWEEN 1 PRECEDING AND CURRENT ROW) AS latest_ts,
                    TIMESTAMP_DIFF(events.created_ts, FIRST_VALUE(created_ts)
                                                      OVER ( ORDER BY event_id
                                                          RANGE BETWEEN 1 PRECEDING AND CURRENT ROW),
                                   SECOND)                         AS delta_in_second
             FROM events) AS deltatable) session ON events.event_id = session.event_id



-- Query for test in console on  https://bigquery.cloud.google.com/
#standardSQL
WITH events AS
         (SELECT 1                               as event_id,
                 TIMESTAMP '2020-01-01 12:00:00' as created_ts,
          UNION ALL
          SELECT 2, TIMESTAMP '2020-01-01 12:01:00',
          UNION ALL
          SELECT 4, TIMESTAMP '2020-01-01 12:06:00',
          UNION ALL
          SELECT 3, TIMESTAMP '2020-01-01 12:05:00',
          UNION ALL
          SELECT 5, TIMESTAMP '2020-01-01 12:08:00')

SELECT events.event_id, events.created_ts, session.session_id
FROM events
         LEFT JOIN (
    SELECT event_id, 1 + COUNTIF(delta_in_second > 60) over (order by event_id) AS session_id
    FROM (
             SELECT event_id,
                    FIRST_VALUE(created_ts)
                    OVER ( ORDER BY event_id
                        RANGE BETWEEN 1 PRECEDING AND CURRENT ROW) AS latest_ts,
                    TIMESTAMP_DIFF(events.created_ts, FIRST_VALUE(created_ts)
                                                      OVER ( ORDER BY event_id
                                                          RANGE BETWEEN 1 PRECEDING AND CURRENT ROW),
                                   SECOND)                         AS delta_in_second
             FROM events) AS deltatable) session ON events.event_id = session.event_id