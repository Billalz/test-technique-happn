from pyspark import SparkContext, SparkConf, SQLContext
from pyspark.sql import types, functions


def main():
    # Spark Configurations
    conf = SparkConf()
    conf.set("spark.master", "local[*]")
    conf = conf.setAppName('job1')
    sc = SparkContext(conf=conf)
    sc.setLogLevel("WARN")
    sql_context = SQLContext(sc)

    # Load adult_data.csv and create dataframe df
    df = sql_context.read.load('data/adult_data.csv',
                               format='com.databricks.spark.csv',
                               header='true',
                               inferSchema='true')

    # Create data frame "df_private" from dataframe "df" filter by worklass equal to "Private"
    df_private = df.where(df.workclass.like('Private'))

    # Count the number of rows of dataframe "df_private"
    count_rows_private = df_private.count()
    print('\nNumber of people have attribut workclass Private:\t', count_rows_private)

    # Aggregate by native country and compute average of age
    print("\nAverage age by native country:")
    df_private.groupby('native-country') \
        .agg(functions.avg('age').cast(types.DecimalType(scale=3)).alias("average-age")).show()


if __name__ == "__main__":
    main()
