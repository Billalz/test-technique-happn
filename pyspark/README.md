
## Quick start

### Requirements
* Python3
* pip
* Spark 2.4

### Running application
 Go to the root project
 ```bash
    cd ./pyspark
```
 Create new env
```bash
    python3 -m venv env
```

Install dependencies

```bash
    pip install -r requirements.txt
```

Run the job
```bash
    spark-submit jobs/average_age_by_country_job.py
```
