/*
1/ Indiquer par pays, le nombre de clients qui n’ont pas effectué de commandes.
Exemple d’output possible :
USA, 4
France, 2
Germany, 1
...
*/

SELECT Country, COUNT(Customers.CustomerID)'Count' FROM Customers LEFT JOIN Orders ON Customers.CustomerID = Orders.CustomerID WHERE OrderID IS NULL GROUP BY Country;


/*
2/ Indiquer l’id produit ainsi que la somme des quantités achetées pour ce produit.
Exemple d’output possible :
30, 35640, 297
14, 222
...
*/

SELECT ProductID, SUM (Quantity) 'Quantity' FROM OrderDetails  GROUP BY ProductID;


/*
3/ Indiquer la moyenne des années de naissance des employés arrondi à l’entier le plus proche.
Exemple d’output possible :
1999
*/

SELECT ROUND(AVG(strftime('%Y', BirthDate))) FROM Employees;


/*
4/ Indiquer les ville et leur pays respectif ainsi que leur nombre de clients dont le nombre de
clients est strictement supérieur à 3.
Exemple d’output possible :
Paris, FRANCE, 45
Madrid, SPAIN, 22
Berlin, GERMANY, 4
...
*/

SELECT City, Country, COUNT(*)AS CountCustomers FROM Customers GROUP BY City HAVING CountCustomers > 3
