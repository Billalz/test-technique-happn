import re

print("Reading file Input.txt...")

ips = []
try:
    input_file = open("Input.txt")
except IOError:
    print('Cannot open Input.txt, please verify the file Input.txt is present.')
    exit(1)
else:
    ips = input_file.readlines()

print("Read file Input.txt is completed")

# Regex ip address IPV4
regex = '^(?=.*[^\.]$)((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.?){4}$'
p = re.compile(regex)

incorrect_ = []
correct_ = []
count = 1
print("Checking ip address...")
for ip in ips:
    if p.match(ip):
        print("Ip{}: {}\t| CORRECT".format(count, ip.strip()))
        correct_.append(ip)
    else:
        print("Ip{}: {}\t| INCORRECT".format(count, ip.strip()))
        incorrect_.append(ip)
    count += 1

print("Saving incorrect ip address in file incorrect_ips.txt...")
file_incorrect_ips = open('incorrect_ips.txt', 'w+')
file_incorrect_ips.writelines(incorrect_)
file_incorrect_ips.close()
print("Saving correct ip address in file correct_ips.txt...")
file_correct_ips = open('correct_ips.txt', 'w+')
file_correct_ips.writelines(correct_)
file_correct_ips.close()
print("-----------------------\n"
      "Terminated with success\n"
      "-----------------------\n"
      "You can find incorrect ip address in file \"incorrect_ips.txt\"  and correct ip address in file \"correct_ips.txt\".")
