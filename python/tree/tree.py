class Tree:
    def __init__(self, number, children=None):
        self.__father = 0
        self._children = []

        assert (isinstance(number, int))
        self.__father = number
        if children is not None:
            self._addchildren(children)

    def __str__(self):
        return ' - '.join(str(x) for x in self.explore())

    def _addchildren(self, trees):
        assert (isinstance(trees, list) or isinstance(trees, tuple))
        assert (len(trees) > 0)
        for tree in trees:
            assert (isinstance(tree, Tree))
            self._children.append(tree)

    def getchildren(self):
        return self._children

    def getfather(self):
        return self.__father

    def explore(self):
        fathers = []
        children = []

        fathers.append(self.__father)
        children.extend(self._children)
        for child in children:
            fathers.append(child.getfather())
            children.extend(child.getchildren())
        return fathers


if __name__ == '__main__':
    tree = Tree(7, [Tree(3, [Tree(1, [Tree(0), Tree(4)]), Tree(8, [Tree(17), Tree(18)])]), Tree(15), Tree(16)])
    print(tree)
